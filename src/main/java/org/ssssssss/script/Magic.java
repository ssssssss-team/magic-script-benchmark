package org.ssssssss.script;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;

import java.io.IOException;

public class Magic extends BaseBenchmark {

	private MagicScript magicScript;

	@Setup
	@Override
	public void setup() throws IOException {
		this.magicScript = MagicScript.create(readScript("/magic.ms"), null);
		this.magicScript.compile();
	}

	@Benchmark
	public void benchmark() {
		MagicScriptContext context = new MagicScriptContext();
		context.putMapIntoContext(getBindings());
		this.magicScript.execute(context);
	}


}
