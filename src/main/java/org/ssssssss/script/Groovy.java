package org.ssssssss.script;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Groovy extends BaseBenchmark {

	private CompiledScript compiledScript;

	@Setup
	@Override
	public void setup() throws Exception {
		ScriptEngineManager factory = new ScriptEngineManager();
		// 每次生成一个engine实例
		Compilable engine = (Compilable) factory.getEngineByName("groovy");
		this.compiledScript = engine.compile(readScript("/script.groovy"));
	}

	@Benchmark
	public void benchmark() throws ScriptException {
		this.compiledScript.eval(getBindings());
	}


}
